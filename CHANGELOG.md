### 18.0.0 Update to Angular 18.
* d31af33 -- [CI/CD] Update packages.json version based on GitLab tag.
*   2a8af17 -- Merge branch 'develop' into 'master'
|\  
| *   474454a -- Merge branch '135-update-to-angular-18' into 'develop'
| |\  
| | * c7eed4d -- Fix test deprecations
| | * 6113c0c -- Override .eslintrc.json
| | * 68664dd -- Additional Angular 18 fixes
| | * 652d6db -- Downgrade package-lock.json
| | * 4352d02 -- Override develop package changes
| |/  
| *   dfae439 -- Merge branch '134-make-roles-selection-not-contain-alredy-assigned-roles' into 'develop'
| |\  
| | * 898e8c7 -- Fix expected params used in tests
| | * 9e381d1 -- Fix spy object in tests
| | * 105c093 -- Downgrade package-lock.json
| | * 4496d08 -- Update uag api version
| | * 0316a9f -- Implement "roles/not-in-group" endpoint
| | * e547fd5 -- Remove already added roles from add roles search
| |/  
|/|   
* | 9de0308 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | a003653 -- [CI/CD] Update packages.json version based on GitLab tag.
* | 5b03b65 -- Merge branch 'develop' into 'master'
|\| 
| *   59a34e6 -- Merge branch 'update-sentinel-versions' into 'develop'
| |\  
| | * d5f71be -- Update versions
| |/  
| *   93871ba -- Merge branch '133-implement-per-id-pagination-size' into 'develop'
| |\  
| | * ed9fd27 -- Update VERSION.txt
| | * 92dc4a3 -- Update VERSION.txt
| | * cde8ebb -- Add per-id pagination
| | * abe0890 -- Rework pagination.service.ts
| |/  
| * 9864106 -- Merge branch 'update-sentinel-components-version' into 'develop'
|/| 
| * 622e00e -- Update sentinel components version
|/  
* 7f63bac -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f43b182 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b661e7 -- Merge branch '132-remove-all-deprecated-sentinel-base-dependencies' into 'master'
|\  
| * d4a175b -- Update all components to work without a deprecated Sentinel directive
|/  
* 0ad9f7d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* abdd0db -- [CI/CD] Update packages.json version based on GitLab tag.
* 049fa20 -- Fix a group edit component material button
* 8f5136a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ee3334e -- [CI/CD] Update packages.json version based on GitLab tag.
*   7df3eeb -- Merge branch '131-update-to-angular-16' into 'master'
|\  
| * 0140234 -- Update to Angular 16
|/  
*   fcb755e -- Merge branch '130-fix-group-edit-user-adding-doesn-t-show-all-options' into 'master'
|\  
| * 3dc06ed -- Increase number of fetched users. Rework sorting technique
|/  
* 42733bf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 16.1.0 Update sentinel versions.
* a003653 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5b03b65 -- Merge branch 'develop' into 'master'
|\  
| *   59a34e6 -- Merge branch 'update-sentinel-versions' into 'develop'
| |\  
| | * d5f71be -- Update versions
| |/  
| *   93871ba -- Merge branch '133-implement-per-id-pagination-size' into 'develop'
| |\  
| | * ed9fd27 -- Update VERSION.txt
| | * 92dc4a3 -- Update VERSION.txt
| | * cde8ebb -- Add per-id pagination
| | * abe0890 -- Rework pagination.service.ts
| |/  
| * 9864106 -- Merge branch 'update-sentinel-components-version' into 'develop'
|/| 
| * 622e00e -- Update sentinel components version
|/  
* 7f63bac -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f43b182 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b661e7 -- Merge branch '132-remove-all-deprecated-sentinel-base-dependencies' into 'master'
|\  
| * d4a175b -- Update all components to work without a deprecated Sentinel directive
|/  
* 0ad9f7d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* abdd0db -- [CI/CD] Update packages.json version based on GitLab tag.
* 049fa20 -- Fix a group edit component material button
* 8f5136a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ee3334e -- [CI/CD] Update packages.json version based on GitLab tag.
*   7df3eeb -- Merge branch '131-update-to-angular-16' into 'master'
|\  
| * 0140234 -- Update to Angular 16
|/  
*   fcb755e -- Merge branch '130-fix-group-edit-user-adding-doesn-t-show-all-options' into 'master'
|\  
| * 3dc06ed -- Increase number of fetched users. Rework sorting technique
|/  
* 42733bf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 16.0.2 Update all components to work without a deprecated Sentinel directive.
* f43b182 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1b661e7 -- Merge branch '132-remove-all-deprecated-sentinel-base-dependencies' into 'master'
|\  
| * d4a175b -- Update all components to work without a deprecated Sentinel directive
|/  
* 0ad9f7d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* abdd0db -- [CI/CD] Update packages.json version based on GitLab tag.
* 049fa20 -- Fix a group edit component material button
* 8f5136a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ee3334e -- [CI/CD] Update packages.json version based on GitLab tag.
*   7df3eeb -- Merge branch '131-update-to-angular-16' into 'master'
|\  
| * 0140234 -- Update to Angular 16
|/  
*   fcb755e -- Merge branch '130-fix-group-edit-user-adding-doesn-t-show-all-options' into 'master'
|\  
| * 3dc06ed -- Increase number of fetched users. Rework sorting technique
|/  
* 42733bf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 16.0.1 Fix a group edit component material button.
* abdd0db -- [CI/CD] Update packages.json version based on GitLab tag.
* 049fa20 -- Fix a group edit component material button
* 8f5136a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ee3334e -- [CI/CD] Update packages.json version based on GitLab tag.
*   7df3eeb -- Merge branch '131-update-to-angular-16' into 'master'
|\  
| * 0140234 -- Update to Angular 16
|/  
*   fcb755e -- Merge branch '130-fix-group-edit-user-adding-doesn-t-show-all-options' into 'master'
|\  
| * 3dc06ed -- Increase number of fetched users. Rework sorting technique
|/  
* 42733bf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* ee3334e -- [CI/CD] Update packages.json version based on GitLab tag.
*   7df3eeb -- Merge branch '131-update-to-angular-16' into 'master'
|\  
| * 0140234 -- Update to Angular 16
|/  
*   fcb755e -- Merge branch '130-fix-group-edit-user-adding-doesn-t-show-all-options' into 'master'
|\  
| * 3dc06ed -- Increase number of fetched users. Rework sorting technique
|/  
* 42733bf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 15.0.0 Update to Angular 15.
* 9229f05 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e481c27 -- Merge branch '129-update-to-angular-15' into 'master'
|\  
| * c25a708 -- Add info about local json server
| * cd1605f -- Adjust code coverage reporter
| *   d824535 -- Merge changes
| |\  
| | * d7702f0 -- Migrate tests
| * | 520db3e -- Migrate tests
| |/  
| * 3125603 -- Update to Angular 15
| * a530e7a -- Update to Angular 15
|/  
* 80c18c9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.5 Add additional sorting options to user, group and microservice tables.
* 6713c48 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3eb41e5 -- Merge branch '128-add-sorting-to-relevant-user-and-group-table-columns' into 'master'
|\  
| * 0981496 -- Resolve "Add sorting to relevant user and group table columns"
|/  
* e0e566a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.4 Added sorting options for group edit and group detail tables. Fixed unintuitive sorting by name in user table.
* 919f34b -- [CI/CD] Update packages.json version based on GitLab tag.
*   6e0b1b7 -- Merge branch 'develop' into 'master'
|\  
| * 537747c -- Develop
|/  
* c934b15 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.3 Fixed expiration date off-by-one error for groups.
* 9fcccf6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ad23219 -- Merge branch '125-incorrect-date-shown-in-group-table' into 'master'
|\  
| * a868e23 -- Fixed off by one error
|/  
* b224078 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.2 Change of name filtering for users by fullname. Add sorting by id.
* b082724 -- [CI/CD] Update packages.json version based on GitLab tag.
*   daef205 -- Merge branch '124-rework-filter-strategy-for-user-table' into 'master'
|\  
| * 2b44225 -- Resolve "Rework filter strategy for user table"
|/  
* 488f595 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.1 Rename kypo2 to kypo.
* cca91ef -- [CI/CD] Update packages.json version based on GitLab tag.
*   231aef1 -- Merge branch '123-rename-kypo2-to-kypo' into 'master'
|\  
| * bec5e7b -- Resolve "Rename kypo2 to kypo"
|/  
* e89c672 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 14.0.0 Update to Angular 14.
* c488a59 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9f18708 -- Merge branch '122-update-to-angular-14' into 'master'
|\  
| * 677e9f2 -- Resolve "Update to Angular 14"
|/  
*   3881379 -- Merge branch 'delete-package-file' into 'master'
|\  
| * a402933 -- Delete muni-kypo-crp-user-and-group-api-13.0.1.tgz
|/  
* 03816d6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 13.2.0 Add button to import users.
* d0105e0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   9451f02 -- Merge branch '121-add-button-to-import-users' into 'master'
|\  
| * f8b81a9 -- Resolve "Add button to import users"
|/  
* 1e8f1a9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 13.1.0 Add export of oidc users.
* d1415d7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   84bd1a5 -- Merge branch '120-create-latest-release' into 'master'
|\  
| * 3aa4717 -- Resolve "Create latest release"
|/  
*   6accaed -- Merge branch '119-rename-get-oidc-data-button' into 'master'
|\  
| * d5b9ec7 -- Resolve "Rename get oidc data button"
|/  
* fcc94d9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 13.0.1 Fix package peer dependencies.
* c67b8c5 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6f94e14 -- Merge branch 'fix-library-peerdependencies' into 'master'
|\  
| * 410758c -- Fix peer dependencies
|/  
* 4152eb7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 13.0.0 Update to Angular 13, CI/CD optimization.
* db947f0 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c5ba949 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * 0bfb3a7 -- 13.0.0 release tag
|/  
*   7946a58 -- Merge branch '118-update-to-angular-13' into 'master'
|\  
| * 205e2a6 -- Resolve "Update to Angular 13"
|/  
* dbe7857 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 12.1.0 Add group and user detail page.
* 0b1ceb4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   e53d2b0 -- Merge branch '114-create-detail-page-for-group' into 'master'
|\  
| * af9e143 -- Resolve "Create detail page for Group"
|/  
*   96052b8 -- Merge branch '117-add-license-file' into 'master'
|\  
| * 153ba43 -- Add license file
|/  
*   c2e6a3a -- Merge branch '116-bump-version-of-sentinel' into 'master'
|\  
| * ab96813 -- Resolve "Bump version of sentinel"
|/  
* 7d216ed -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 12.0.4 Add build of example app to CI. Rename endpoints from kypo2 to kypo. Save user preferred pagination. Fix problem with unsaved changes on discard. Add aliases to config. Remove roles count and members count column from group table.
* f2f22ea -- [CI/CD] Update packages.json version based on GitLab tag.
*   669a7c8 -- Merge branch '115-create-tag-with-latest-changes' into 'master'
|\  
| * f0acbff -- Tag message
|/  
*   1e93e8b -- Merge branch '113-remove-roles-count-and-members-count-from-group-overview-table' into 'master'
|\  
| * 4cd21b1 -- Removed the Roles Count and Members Count colunm from group table.
|/  
*   6954e08 -- Merge branch '112-add-node_modules-to-gitignore' into 'master'
|\  
| * 8ed43db -- Add node_modules to gitignore
|/  
*   c27734a -- Merge branch '111-add-aliases-to-tsconfig' into 'master'
|\  
| * 7f5f632 -- Add aliases to tsconfig
|/  
*   1818896 -- Merge branch '109-fix-error-message-for-unsaved-changes-on-component-reload' into 'master'
|\  
| * cdd651a -- Fix unsaved changes on discard problem
|/  
*   db1d40a -- Merge branch '106-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 6b3cf1a -- Resolve "Save user preferences of pagination page size"
|/  
*   ccb9adf -- Merge branch '108-bump-version-of-sentinel' into 'master'
|\  
| * fbe79aa -- Bump version of sentinel
|/  
*   62c8d97 -- Merge branch '107-fix-local-config-paths' into 'master'
|\  
| * 9e6247f -- Fix paths
|/  
*   5992b3c -- Merge branch '105-add-build-example-app-to-ci' into 'master'
|\  
| * ab28325 -- Add build example app to CI
|/  
* 90886ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
### 12.0.3 Update gitlab CI
* 2949a0a -- [CI/CD] Update packages.json version based on GitLab tag.
*   861305c -- Merge branch '104-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * bc52f93 -- Update gitlab CI
|/  
* 8f0af5f -- Update project package.json version based on GitLab tag. Done by CI
*   b3b075f -- Merge branch '103-bump-sentinel-layout-version' into 'master'
|\  
| * 57597a7 -- Bump sentinel layout
|/  
* 663594c -- Update project package.json version based on GitLab tag. Done by CI
*   daae4ce -- Merge branch '102-bump-version-of-sentinel' into 'master'
|\  
| * f3973c7 -- Resolve "Bump version of Sentinel"
|/  
* 7df6bf4 -- Update project package.json version based on GitLab tag. Done by CI
*   af93d29 -- Merge branch '101-update-to-angular-12' into 'master'
|\  
| * 861c1d9 -- Resolve "Update to Angular 12"
|/  
*   0b478ad -- Merge branch '100-update-oidc-configuration' into 'master'
|\  
| * 8d7c014 -- Resolve "Update oidc configuration"
|/  
* 1bba0b0 -- Update project package.json version based on GitLab tag. Done by CI
*   5990b0b -- Merge branch '99-update-to-angular-11' into 'master'
|\  
| * ec4dd77 -- Resolve "Update to Angular 11"
|/  
*   f1929d6 -- Merge branch '98-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 8ab7229 -- Migrate to eslint, fix errors/warnings
|/  
*   daff81b -- Merge branch '97-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 167f8ac -- recreate package lock
|/  
* af039e1 -- Update project package.json version based on GitLab tag. Done by CI
*   7608d34 -- Merge branch '96-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 92ba915 -- Rename package and update dependencies
|/  
* 4a4a872 -- Update project package.json version based on GitLab tag. Done by CI
*   34ba884 -- Merge branch '445720-master-patch-18186' into 'master'
|\  
| * f712ba8 -- Update projects/kypo-user-and-group-agenda/package.json
|/  
*   cab4d54 -- Merge branch '95-update-dependencies-in-example-app' into 'master'
|\  
| * 4ff05d6 -- Update example app dependencies
|/  
*   b273613 -- Merge branch '94-update-dependencies-to-new-format' into 'master'
|\  
| * cce74f6 -- Rename dependencies
|/  
* 75553a6 -- Update project package.json version based on GitLab tag. Done by CI
*   9236e49 -- Merge branch '93-rename-package-to-kypo-user-and-group-agenda' into 'master'
|\  
| * 02ff9b3 -- Rename package
|/  
*   980c174 -- Merge branch '91-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 3df4d8c -- Remove personal info from README
|/  
*   1a97242 -- Merge branch '90-replace-kypo-auth-with-sentinel-auth-in-example-app' into 'master'
|\  
| * 528e2d2 -- Resolve "Replace kypo-auth with @sentinel/auth in example app"
|/  
* 4b2e4fb -- Update project package.json version based on GitLab tag. Done by CI
*   7963e22 -- Merge branch '84-add-microservice-overview-page' into 'master'
|\  
| * 19f1a77 -- Resolve "Add microservice overview page"
|/  
*   5a59241 -- Merge branch '89-use-cypress-docker-image-in-ci' into 'master'
|\  
| * ab56500 -- Resolve "Use cypress docker image in CI"
|/  
* d788538 -- Update project package.json version based on GitLab tag. Done by CI
*   7b9f265 -- Merge branch '88-fix-example-app-build' into 'master'
|\  
| * 5173563 -- Optimize paths for tests and example app
|/  
*   c3d4619 -- Merge branch '87-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 44d2e9a -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 70f869b -- Update project package.json version based on GitLab tag. Done by CI
*   b7a81a1 -- Merge branch '86-refactor-to-use-sentinel-components' into 'master'
|\  
| * f031ed6 -- Refactor to use sentinel components, layout and common instead of the kypo ones
|/  
* 25c989a -- Update project package.json version based on GitLab tag. Done by CI
* c3c6ee5 -- Merge branch '85-update-to-angular-10' into 'master'
* b9b1965 -- Resolve "Update to Angular 10"
